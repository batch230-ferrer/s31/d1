
// The "http module" let Node.js transfer data using Hyper Text Transfer Protocol - HTTP
    // it can create an HTTP server that listens to server ports such as..
    // 3000, 4000, 5000, 8000 (Usually used for web development)

// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications

// The "http module" 

// Using the module's createServer() method, we can create an HTTP server that listens to request on a specfieied port and gives responses back to the client
// createServer() is a method of the http object responsible for creating a server using Node.js

const http = require("http");

let port = 4000;

const server = http.createServer((request, response) =>{

    if(request.url == '/greeting'){
        // method of the response object that allows us to set status codes and content types.
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.end('Hello world.');
    }

    else if(request.url == '/homepage'){
        response.writeHead(200, {'Content-Type' : 'text/plain'});
        response.end('This is the homepage');
    }
    else{
        // Set a status code for response - 404 means not Found
        response.writeHead(404, {'Content-type' : 'text/plain'});
        response.end('404 Page not available');
    }

});

server.listen(port);

console.log('Server now accessible at localhost' + port);