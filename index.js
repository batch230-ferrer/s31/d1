// use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that cointains one or more routines
// The "http module" let Node.js transfer data using Hyper Text Transfer Protocol - HTTP
    // it can create an HTTP server that listens to server ports such as..
        // 3000, 4000, 5000, 8000 (Usually used for web development)
// The "http module" is a set of individual files that contain code to create a "component" that helps establish data transfer between applications
// Clients (devices/browsers) and server (nodeJS/expressJS application) communicate by exchanging individual messages (request/response)
// Request - the messages sent by the client
// Response - the message send by the serves as response

let http = require("http");

// Using the module's createServer() method, we can create an HTTP server that listens to request on a specfieied port and gives responses back to the client
// createServer() is a method of the http object responsible for creating a server using Node.js

http.createServer(function (request, response){

    // Use the wrideHead() method to:
    // Set a status code for the response - a 200 means OK
    // Set the content-type of the response - as a plain text message
    response.writeHead(200, {'Content-type': 'text/plain'})

    // Send the response with the content 'Hello Sekai"
    response.end('Hello Sekai');

}).listen(4000);
// A port is a virtual point where network connections start and end.
// The server willl be assigned to port 4000 via the listen() method.
    // Where  the server will listen to any request that sent to it and will also sent the response via this port


// When server is running, console will print the message:
console.log('Server is running at localhost: 4000');
// the messages or outputs from using console.log will now be displahyed to the terminal